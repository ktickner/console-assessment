/**
 * Example problem with existing solution and passing test.
 * See problem 0 in the spec file for the assertion
 * @returns {string}
 */
exports.example = () => {
    return 'hello world';
};

exports.stripPrivateProperties = (privateProps, users) => {
    return users.map((user) => {
        privateProps.forEach((prop) => {
            delete user[prop];
        });
        
        return user;
    });
};

exports.excludeByProperty = (excludedProp, users) => {
    return users.filter((user) => {
        return !Object.keys(user).includes(excludedProp);
    });
};

exports.sumDeep = (objectArray) => {
    return objectArray.map((object) => {
        return {
            objects: object.objects.reduce((accumulator, { val }) => {
                return accumulator + val;
            }, 0),
        };
    });
};

exports.applyStatusColor = (statusColors, statusObjects) => {
    return statusObjects.map((status) => {
        for (const color in statusColors) {
            if (statusColors[color].includes(status.status)) {
                status.color = color;
            }
        }

        return status;
    }).filter((status) => !!status.color);
};

exports.createGreeting = (greetFunc, phrase) => {
    return name => greetFunc(phrase, name);
};

exports.setDefaults = (defaults) => {
    return user => Object.assign({}, defaults, user);
};

// The requirements for this one are vague so I've tried to account for a use case where anything goes
// Ideally I would prefer to throw errors on records that are incorrectly created (i.e. if the month joined is missing)
exports.sanitizeUser = (user) => {
    const sanitizeString = (string) => {
        return (typeof string === 'string' || string instanceof String);
    };
    
    const sanitizeAddress = ({ num, street, suburb }) => {
        // Accounts for any combination of address properties
        const sanitizedNum = (!isNaN(+num) ? `${num}` : '');
        const sanitizedStreet = (sanitizeString(street) ? street : '');
        const sanitizedSuburb = (sanitizeString(suburb) ? suburb : '');
        const space = (sanitizedNum !== '' && sanitizedStreet !== '' ? ' ' : '');
        const comma = (sanitizedSuburb !== '' && (sanitizedStreet !== '' || sanitizedNum !== '') ? ', ' : '');
        
        return `${ sanitizedNum }${ space }${ sanitizedStreet }${ comma }${ sanitizedSuburb }`;
    };
    
    const sanitizeMonth = (month) => {
        if (!isNaN(+month)) {
            return month + 1;
        }
        
        // Accounting for cases where the monthJoined returns as 'Jan', for example
        if (sanitizeString(month)) {
            return month;
        }
        
        return '';
    };
    
    const sanitizeName = (name) => {
        if (!sanitizeString(name)) {
            return '';
        }
        
        return (!!user.name.split(' ')[0] ? user.name.split(' ')[0] : '');
    };
    
    return Object.assign({}, user, {
        firstName: sanitizeName(user.name),
        monthJoined: sanitizeMonth(user.monthJoined),
        fullAddress: sanitizeAddress(user.address),
    });
};

